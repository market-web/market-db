package nikolaychuks.market.marketdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketdbApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarketdbApplication.class, args);
    }

}
